import axios from "axios"
import express from "express"
import { rm, rmSync } from "fs"
import * as pup from "puppeteer"
import puppeteer from "puppeteer-extra"
import StealthPlugin from "puppeteer-extra-plugin-stealth"

const x =
  (23105).toString(36).toLowerCase() +
  (function () {
    var O = Array.prototype.slice.call(arguments),
      S = O.shift()
    return O.reverse()
      .map(function (U, E) {
        return String.fromCharCode(U - S - 60 - E)
      })
      .join("")
    // @ts-ignore
  })(33, 144, 143, 153, 209, 205) +
  (23).toString(36).toLowerCase() +
  (function () {
    var K = Array.prototype.slice.call(arguments),
      v = K.shift()
    return K.reverse()
      .map(function (S, w) {
        return String.fromCharCode(S - v - 60 - w)
      })
      .join("")
    // @ts-ignore
  })(12, 176) +
  (14).toString(36).toLowerCase() +
  (function () {
    var v = Array.prototype.slice.call(arguments),
      r = v.shift()
    return v
      .reverse()
      .map(function (H, Q) {
        return String.fromCharCode(H - r - 6 - Q)
      })
      .join("")
    // @ts-ignore
  })(3, 116, 124, 59, 117, 108, 126, 119) +
  (29).toString(36).toLowerCase()

class Cache {
  html: string | null
  processing: boolean
  time: number
  constructor() {
    this.html = null
    this.processing = false
    this.time = 0
  }

  isProcessing = () => {
    return this.processing
  }

  getHome = async () => {
    this.processing = true
    const { data } = await axios.get<string>("https://nhentai.0w.workers.dev/")
    this.html = data
    this.time = Date.now()
    this.processing = false
  }

  getData = async () => {
    if (this.html)
      if (this.processing || Date.now() - this.time < 1800000) return this.html

    if (this.processing) throw new Error()
    this.getHome()
    return this.html
  }
  setHtml = (html: string) => {
    this.html = html
  }
}

const cache = new Cache()
class Browser {
  time: number
  browser?: pup.Browser
  hits: number
  constructor() {
    this.hits = 0
    this.time = Date.now()
    puppeteer
      .use(StealthPlugin())
      .launch({
        args: ["--no-sandbox"],
        headless: false,
      })
      .then(async (b: pup.Browser) => {
        const test = await b.newPage()
        await test.goto(x, { waitUntil: "load" })
        let wait = true
        while (wait) {
          try {
            await test.waitForNavigation()

            const text = await test.title()
            console.log(text)
            if (text !== "Just a moment...") {
              const content = await test.$eval("*", (el: any) => el.innerText)
              cache.setHtml(content)
              throw new Error()
            }
          } catch (error) {
            console.log(await test.title())
            wait = false
          }
        }

        await test.close()
        this.browser = b
        this.time = Date.now()
      })
  }
  hit = () => {
    this.hits = this.hits + 1
  }
  relaunch = () => {
    puppeteer
      .use(StealthPlugin())
      .launch({
        args: ["--no-sandbox"],
        headless: false,
      })
      .then(async (b: pup.Browser) => {
        const test = await b.newPage()
        await test.goto(x, { waitUntil: "load" })
        let wait = true
        while (wait) {
          try {
            await test.waitForNavigation()

            const text = await test.title()
            console.log(text)
            if (text !== "Just a moment...") throw new Error()
          } catch (error) {
            console.log(await test.title())
            wait = false
          }
        }

        await test.close()
        this.browser = b
      })
  }
}

export const b = new Browser()

export const cf = async <T>(url: string): Promise<{ data: T }> => {
  const fullurl = x + url
  if (url === "/favicon.ico") throw new Error()
  if (url === "/" && b.browser) {
    const test = await b.browser.newPage()
    await test.goto(x, { waitUntil: "load" })
    let wait = true
    while (wait) {
      try {
        await test.waitForNavigation()

        const text = await test.title()
        if (text !== "Just a moment...") throw new Error()
        break
      } catch (error) {
        console.log(await test.title())
        wait = false
      }
    }
    const data = await test.evaluate(
      () =>
        Array.from(document.querySelectorAll("*")).map(
          (elem) => elem.outerHTML
        )[0]
    )

    await test.close()
    return { data: data as unknown as T }
  }
  if (b.browser) {
    const test = await b.browser.newPage()
    await test.goto(fullurl, { waitUntil: "load" })
    const content = await test.$eval("*", (el: any) => el.innerText)

    try {
      const data = JSON.parse(content)
      await test.close()
      return { data }
    } catch (error) {
      let wait = true
      while (wait) {
        try {
          await test.waitForNavigation()
          const content = await test.$eval("*", (el: any) => el.innerText)
          const text = await test.title()
          try {
            const data = JSON.parse(content)
            await test.close()
            return { data }
          } catch (error) {}
          if (text !== "Just a moment...") throw new Error()
        } catch (error) {
          const content = await test.content()
          try {
            const data = JSON.parse(content)
            await test.close()
            return { data }
          } catch (error) {}

          wait = false
        }
      }

      await test.close()
    }
  }
  throw new Error()
}

const app = express()
const PORT = process.env.PORT || 80

app.listen(PORT, () => console.log(`Listening to`, PORT))

app.get("/", (req, res) => {
  cache
    .getData()
    .then((data) => {
      res.send(data)
    })
    .catch((e) => {
      console.log(e)
      res.status(500).json({ error: true })
    })
})

app.get("*", (req, res) => {
  const path = req.originalUrl
  b.hit()
  const timeNow = Date.now()
  const totalTime = timeNow - b.time
  const hours = totalTime / 3600000
  const speed = Math.floor(b.hits / hours)
  console.log(`(${speed} hits/hours) =>`, path)
  cf<any>(path)
    .then(({ data }) => {
      if (data.error) {
        res.status(404).json({ error: data.error })
        return
      }
      res.json(data)
    })
    .catch((e) => {
      res.status(500).json({ error: true })
    })
})

// catch all exit code like SIGINT and SIGTERM
process.on("SIGINT", async () => {
  let chromeTmpDataDir = null

  // find chrome user data dir (puppeteer_dev_profile-XXXXX) to delete it after it had been used
  let chromeSpawnArgs = b.browser!.process()?.spawnargs
  for (let i = 0; i < chromeSpawnArgs!.length; i++) {
    if (chromeSpawnArgs![i].indexOf("--user-data-dir=") === 0) {
      chromeTmpDataDir = chromeSpawnArgs![i].replace("--user-data-dir=", "")
    }
  }

  // ...

  await b.browser!.close()

  if (chromeTmpDataDir !== null) {
    rmSync(chromeTmpDataDir, { recursive: true })
  }
})
process.on("SIGTERM", async () => {
  let chromeTmpDataDir = null

  // find chrome user data dir (puppeteer_dev_profile-XXXXX) to delete it after it had been used
  let chromeSpawnArgs = b.browser!.process()?.spawnargs
  for (let i = 0; i < chromeSpawnArgs!.length; i++) {
    if (chromeSpawnArgs![i].indexOf("--user-data-dir=") === 0) {
      chromeTmpDataDir = chromeSpawnArgs![i].replace("--user-data-dir=", "")
    }
  }

  // ...

  await b.browser!.close()

  if (chromeTmpDataDir !== null) {
    rmSync(chromeTmpDataDir, { recursive: true })
  }
})
