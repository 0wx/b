require('dotenv/config')

module.exports = {
  apps: [
    {
      name: "project-name",
      exec_mode: "cluster",
      instances: "1",
//      cron_restart: '0 * * * *',
      script: "./build/index.js", 
      env: {...process.env, DISPLAY: ':1', PORT: '80'},
    },
  ],
};
